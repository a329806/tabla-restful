const express = require('express');

function sumar(req, res, next) {
  let n1 = parseFloat(req.params.n1);
  let n2 = parseFloat(req.params.n2);
  let result = parseFloat(n1+n2);
  res.send(`La suma de ${n1} + ${n2} es ${result}`);
}

function multiplicar(req, res, next) {
  let n1 = parseFloat(req.body.n1);
  let n2 = parseFloat(req.body.n2);
  let result = parseFloat(n1*n2);
  res.send(`La multiplicacion de ${n1} * ${n2} es ${result}`);
}

function dividir(req, res, next) {
  let n1 = parseFloat(req.body.n1);
  let n2 = parseFloat(req.body.n2);
  if(n2 == 0){
    res.send('No se puede dividir entre 0');
  } else {
    let result = parseFloat(n1/n2);
    res.send(`La division de ${n1} / ${n2} es ${result}`);
  }
}

function potencia(req, res, next) {
  let n1 = parseFloat(req.body.n1);
  let n2 = parseFloat(req.body.n2);
  let result = parseFloat(Math.pow(n1, n2));
  res.send(`La potencia de ${n1} ^ ${n2} es ${result}`);
}

function restar(req, res, next) {
  let n1 = parseFloat(req.params.n1);
  let n2 = parseFloat(req.params.n2);
  let result = parseFloat(n1-n2);
  res.send(`La resta de ${n1} - ${n2} es ${result}`);
}

module.exports = {
  sumar, multiplicar, dividir, potencia, restar
}
