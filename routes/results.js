const express = require('express');
const router = express.Router();
const controller = require('../controllers/results');

/*  Lista especificada como en la tarea,
    en donde no venia en los parametros lo
    estoy pasando por el body.*/
router.get('/:n1/:n2', controller.sumar);
router.post('/', controller.multiplicar);
router.put('/', controller.dividir);
router.patch('/', controller.potencia);
router.delete('/:n1/:n2', controller.restar);

module.exports = router;
